
  $(window).load(function(){
    var flexsliderConf = {
      animation: "slide",
      animationLoop: false,
      itemWidth: 320,
      itemMargin: 0,
      minItems: 1,
      maxItems: 3
    }

  $('.flexslider').flexslider(flexsliderConf);
});