var express = require('express');
var router = express.Router();

var data = {
  title : 'VJ Prototypes',
  pages : [
    {
      'letter' : 'A',
      'title' : 'Autumn campaign',
      'journeys' : [{
          'title' : 'Lorem ipsum',
          'url_title' : 'A.1',
          'url' : 'a-1/'
        }, {
          'title' : 'Lorem ipsum',
          'url_title' : 'A.2',
          'url' : 'a-2/'
        }, {
          'title' : 'Mobile version',
          'url_title' : 'iPhone 5',
          'url' : 'a-1-mobile/'
        }
      ]
    }, {
      'letter' : 'B',
      'title' : 'Test',
      'journeys' : [{
          'title' : 'Lorem ipsum',
          'url_title' : 'B.1',
          'url' : 'a-1/'
        }, {
          'title' : 'Lorem ipsum',
          'url_title' : 'B.2',
          'url' : 'a-2/'
        }, {
          'title' : 'Mobile version',
          'url_title' : 'iPhone 5',
          'url' : 'a-1-mobile/'
        }
      ]
    }, {
      'letter' : 'C',
      'title' : 'Test',
      'journeys' : [{
          'title' : 'Lorem ipsum',
          'url_title' : 'C.1',
          'url' : 'a-1/'
        }, {
          'title' : 'Lorem ipsum',
          'url_title' : 'C.2',
          'url' : 'a-2/'
        }, {
          'title' : 'Mobile version',
          'url_title' : 'iPhone 5',
          'url' : 'a-1-mobile/'
        }
      ]
    }, {
      'letter' : 'D',
      'title' : 'Test',
      'journeys' : [{
          'title' : 'Lorem ipsum',
          'url_title' : 'D.1',
          'url' : 'a-1/'
        }, {
          'title' : 'Lorem ipsum',
          'url_title' : 'D.2',
          'url' : 'a-2/'
        }, {
          'title' : 'Mobile version',
          'url_title' : 'iPhone 5',
          'url' : 'a-1-mobile/'
        }
      ]
    }
  ]
};

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', data);
});
/* GET autumn-campaign. */
router.get('/a-1', function (req, res) {
  res.render('autumn-campaign/index-1', { 'page' : 'Autumn campaign', 'journey' : 'A.1', 'where_next': '/a-2', data });
});
router.get('/a-2', function (req, res) {
  res.render('autumn-campaign/index-2', { 'page' : 'Autumn campaign', 'journey' : 'A.2', 'where_next': '', 'where_prev': '/a-1', data });
});
router.get('/a-1-mobile', function (req, res) {
  res.render('autumn-campaign/index-mobile', { 'page' : 'Autumn campaign', 'journey' : 'Autumn campaign', data });
});

module.exports = router;
