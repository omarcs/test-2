var gulp = require('gulp'),
  nodemon = require('gulp-nodemon'),
  plumber = require('gulp-plumber'),
  livereload = require('gulp-livereload'),
  sass = require('gulp-ruby-sass');

gulp.task('sass-zoocha', function () {
  return sass('./public/zoocha-theme/**/*.scss')
    .pipe(gulp.dest('./public/zoocha-theme'))
    .pipe(livereload());
});

gulp.task('sass-prototype', function () {
  return sass('./public/prototype-theme/**/*.scss')
    .pipe(gulp.dest('./public/prototype-theme'))
    .pipe(livereload());
});

gulp.task('sass-bootstrap', function () {
  return sass('./public/bootstrap/*.scss')
    .pipe(gulp.dest('./public/bootstrap'))
    .pipe(livereload());
});

gulp.task('watch', function() {
  gulp.watch('./public/zoocha-theme/**/*.scss', ['sass-zoocha']);
  gulp.watch('./public/prototype-theme/**/*.scss', ['sass-prototype']);
  gulp.watch('./public/bootstrap/*.scss', ['sass-bootstrap']);
});

gulp.task('develop', function () {
  livereload.listen();
  nodemon({
    script: 'bin/www',
    ext: 'js nunjucks coffee',
    stdout: false
  }).on('readable', function () {
    this.stdout.on('data', function (chunk) {
      if(/^Express server listening on port/.test(chunk)){
        livereload.changed(__dirname);
      }
    });
    this.stdout.pipe(process.stdout);
    this.stderr.pipe(process.stderr);
  });
});

gulp.task('default', [
  'sass-zoocha',
  'sass-prototype',
  'sass-bootstrap',
  'develop',
  'watch'
]);
